# database properties
DB_NAME = ""
USER = ""
PASSWORD = ""
HOST = "localhost"
PORT = 3306

# mail properties
MAIL_SERVER = "<enter_imap_server>"
MAIL_PORT = 993
MAIL_BOX = "<enter_login>"
MAIL_PASSWORD = "<enter_password>"