#!/usr/bin/env python
# coding=utf-8
import email
import imaplib
import datetime
import time
import mysql.connector
import re
import settings


# подключение к базе данных
def connect_db():
    cnx = mysql.connector.connect(user=settings.USER, database=settings.DB_NAME, password=settings.PASSWORD, host=settings.HOST, port=settings.PORT)
    cursor = cnx.cursor()

    return cnx, cursor


# возвращает текст сообщения
def get_decoded_email_body(message_body):

    msg = email.message_from_string(message_body)

    text = ""
    if msg.is_multipart():
        html = None
        for part in msg.get_payload():

            if part.get_content_charset() is None:
                text = part.get_payload(decode=True)
                continue

            charset = part.get_content_charset()

            if part.get_content_type() == 'text/plain':
                text = unicode(part.get_payload(decode=True), str(charset), "ignore").encode('utf8', 'replace')

            if part.get_content_type() == 'text/html':
                html = unicode(part.get_payload(decode=True), str(charset), "ignore").encode('utf8', 'replace')

        if text is not None:
            return text.strip()
        else:
            return html.strip()
    else:
        charset = msg.get_content_charset()
        # если не установлена кодировка, используем utf-8
        if charset is None:
            charset = 'utf-8'
        text = unicode(msg.get_payload(decode=True), charset, 'ignore').encode('utf8', 'replace')
        return text.strip()


# составить словарь с пакетами, пришедшими в новых письмах
def get_packages_from_mail():

    packages_count_dictionary = {}

    mailbox = imaplib.IMAP4_SSL(settings.MAIL_SERVER, settings.MAIL_PORT)

    # подключаемся к почтовму серверу
    mailbox.login(settings.MAIL_BOX, settings.MAIL_PASSWORD)
    mailbox.select()

    # берем только непрочитанные сообщения
    typ, data = mailbox.search(None, 'UNSEEN')

    for num in data[0].split():
        typ, data = mailbox.fetch(num, '(RFC822)')

        text = get_decoded_email_body(data[0][1])
        text_rows = text.split()

        try:
            # первая строка должна быть timestamp'ом
            timestamp = datetime.datetime.utcfromtimestamp(int(text_rows[0]))
            date = str(timestamp.date())
        except:
            continue

        # последующие строки - имена пакетов и имена проектов, разделенные '||'
        for item in text_rows[1:]:
            split_package = item.split('||')
            if len(split_package) != 2:
                continue

            package = split_package[0]

            # с конца отсчитываем три '-', все что слева - имя пакета
            split_package_name = package.rsplit('-', 4)

            package_name = split_package_name[0]
            project = split_package[1]

            #добавляем запись в словарь или увеличиваем счетчик
            tuple_dict = (date, package_name, project, get_package_type(package)) #последний параметр - тип архитектуры
            if tuple_dict in packages_count_dictionary:
                packages_count_dictionary[tuple_dict] += 1
            else:
                packages_count_dictionary[tuple_dict] = 1

    mailbox.close()
    mailbox.logout()

    return packages_count_dictionary

# добавление нового апдейта
def add_update(cursor):

    current_date = datetime.datetime.now()
    insert_update = ("INSERT INTO updates (Date) VALUES (%(Date)s)")
    insert_update_data = {'Date': current_date}

    cursor.execute(insert_update, insert_update_data)
    update_id = cursor.lastrowid

    return update_id


# добавление в базу информации о новых пакетах
def add_packages(cursor, update_id, packages_count_dictionary):

    # идем по словарю и добавлеям информацию в базу
    for (date, package, project, package_type) in packages_count_dictionary:
        # количество запусков пакета в кокретную дату
        count = packages_count_dictionary[(date, package, project, package_type)]

        # ищем, есть ли уже такой паекет в базе
        select_package = ("SELECT Id from package where Name = %(Name)s")
        select_package_data = {'Name': package}

        cursor.execute(select_package, select_package_data)
        row_id = cursor.fetchone()

        # если пает был, берем его id, а если нет - добавляем его в таблицу package
        if row_id is None:

            insert_package = ("INSERT INTO package (Name, Is86, Project) VALUES (%(Name)s, %(Is86)s, %(Project)s)")
            insert_package_data = {'Name': package, 'Is86': package_type, 'Project': project}

            cursor.execute(insert_package, insert_package_data)
            package_id = cursor.lastrowid
        else:
            package_id = row_id[0]

        #если уже есть результаты для этого пакета, апдейта и даты, то добавляем к счетчику; иначе создаем новую запись
        select_result = ("SELECT Count from results where PackageId = %(PackageId)s and UpdateId = %(UpdateId)s and StatDate = %(StatDate)s")
        select_result_data = {'PackageId': package_id, 'UpdateId': update_id, 'StatDate': date}

        cursor.execute(select_result, select_result_data)
        row_count = cursor.fetchone()

        if row_count is None:
            insert_result = ("INSERT INTO results (PackageId, UpdateId, Count, StatDate) VALUES (%(PackageId)s, %(UpdateId)s, %(Count)s, %(StatDate)s)")
            insert_result_data = {'PackageId': package_id, 'UpdateId': update_id, 'Count': count, 'StatDate': date}

            cursor.execute(insert_result, insert_result_data)
        else:
            count += row_count[0]

            update_result = ("UPDATE results SET Count = %(Count)s where PackageId = %(PackageId)s and UpdateId = %(UpdateId)s and %(StatDate)s")
            update_result_data = {'Count': count, 'PackageId': package_id, 'UpdateId': update_id, 'StatDate': date}

            cursor.execute(update_result, update_result_data)


# добвавляет время обновления
def fix_duration(cursor, update_id, duration):

    update_duration = ("UPDATE updates SET Duration = %(Duration)s WHERE Id = %(Id)s")
    update_duration_data = {'Duration': duration, 'Id': update_id}

    cursor.execute(update_duration, update_duration_data)

# парсит архитектуру пакета
def get_package_type(package_name):
    noarch_ind = package_name.rfind("noarch")

    match = re.search("(i[0-9]86)", package_name)
    if match is not None:
        i586_ind = match.lastindex
    else:
        i586_ind = -1

    match1 = re.search("(amd64|x86_64)", package_name)
    if match1 is not None:
        amd_ind = match1.lastindex
    else:
        amd_ind = -1

    max_ind = max((noarch_ind, amd_ind, i586_ind))

    if max_ind == noarch_ind:
        return None
    elif max_ind == amd_ind:
        return False
    else:
        return True

def update():

    # регистрируем время начала обновления
    update_start_time = time.time()

    # получаем информацию с почты
    packages_count_dictionary = get_packages_from_mail()

    # подключаемся к базе
    try:
        (cnx, cursor) = connect_db()
    except Exception as e:
        print e
        exit(-1)

    # начинаем транзакцию
    cnx.start_transaction()

    #добавлеяем новый апдейт и новую информацию о пакетах
    try:
        update_id = add_update(cursor)
        add_packages(cursor, update_id, packages_count_dictionary)
    except Exception as e:
        print e
        cnx.rollback()
        exit(-1)

    # регистрируем время конца обновления и добавяем его в базу
    update_end_time = time.time()
    duration = update_end_time - update_start_time

    try:
        fix_duration(cursor, update_id, duration)
    except Exception as e:
        print e
        cnx.rollback()
        exit(-1)

    #коммитим изменения
    cnx.commit()
    cursor.close()
    cnx.close()

    exit(0)


if __name__ == '__main__':
    update()